import 'package:bloc_example/src/constants.dart';
import 'package:bloc_example/src/domain/network/netowork_creator.dart';
import 'package:bloc_example/src/modules/app/app.dart';
import 'package:flutter/material.dart';
import 'src/domain/services/storage.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await configureDependencies();
  runApp(const App());
}

Future<void> configureDependencies() async {
  serviceLocator.registerSingleton(await Storage.create());
  serviceLocator.registerSingleton(await NetworkCreator.create());
}
