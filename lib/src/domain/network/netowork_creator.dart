import 'dart:io';

import 'package:bloc_example/src/constants.dart';
import 'package:bloc_example/src/domain/services/storage.dart';
import 'package:dio/dio.dart';

import 'interfaces/base_client_generator.dart';
import 'interfaces/network_options.dart';

class NetworkCreator {
  final Dio _client;

  const NetworkCreator(this._client);

  Dio get client => _client;

  static Future<NetworkCreator> create() async {
    final client = Dio();
    final storage = serviceLocator<Storage>();
    client.interceptors.add(InterceptorsWrapper(onRequest: (options, handler) {
      options.headers['Authorization'] = 'Bearer ${storage.get(authSaveKey)}';
      handler.next(options);
    }));
    return NetworkCreator(client);
  }

  Future<Response> request(
      {required BaseClientGenerator route, NetworkOptions? options}) {
    return _client.fetch(RequestOptions(
        baseUrl: route.baseURL,
        method: route.method,
        path: route.path,
        queryParameters: route.queryParameters,
        data: route.body,
        sendTimeout: route.sendTimeout,
        receiveTimeout: route.sendTimeout,
        onReceiveProgress: options?.onReceiveProgress,
        validateStatus: (statusCode) => (statusCode! >= HttpStatus.ok &&
            statusCode <= HttpStatus.multipleChoices)));
  }
}
