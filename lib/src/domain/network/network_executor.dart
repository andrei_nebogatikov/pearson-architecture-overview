import 'package:bloc_example/src/constants.dart';
import 'package:bloc_example/src/domain/network/result.dart';
import 'package:dio/dio.dart';

import 'connectivity.dart';
import 'error.dart';
import 'interfaces/base_client_generator.dart';
import 'interfaces/network_options.dart';
import 'netowork_creator.dart';
import 'network_decoding.dart';

class NetworkExecuter {
  static var shared = NetworkExecuter();

  bool debugMode = true;

  final _creator = serviceLocator<NetworkCreator>();

  Future<Result<K, NetworkError>> execute<T, K>(
      {required BaseClientGenerator route,
      required T Function(Map<String, dynamic>) parser,
      NetworkOptions? options}) async {
    if (debugMode) {
      print(route);
    }
    if (await NetworkConnectivity.status) {
      try {
        var response = await _creator.request(route: route, options: options);
        var data = NetworkDecoding.shared.decode<T, K>(
            response: response, parser: parser);
        return Result.success(data);
      } on DioError catch (diorError) {
        if (debugMode) {
          print('$route => ${NetworkError.request(error: diorError)}');
        }
        return Result.failure(NetworkError.request(error: diorError));
      } on TypeError catch (e) {
        if (debugMode) {
          print('$route => ${NetworkError.type(error: e.toString())}');
        }
        return Result.failure(NetworkError.type(error: e.toString()));
      } on EmptyResult {
        return const Result.failure(NetworkError.empty());
      }

      // No Internet Connection
    } else {
      if (debugMode) {
        print(
            const NetworkError.connectivity(message: 'No Internet Connection'));
      }
      return const Result.failure(
          NetworkError.connectivity(message: 'No Internet Connection'));
    }
  }
}
