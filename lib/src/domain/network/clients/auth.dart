import 'package:bloc_example/src/constants.dart';
import 'package:bloc_example/src/domain/network/interfaces/base_client_generator.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'auth.freezed.dart';

@freezed
class AuthenticationNetworkClient extends BaseClientGenerator
    with _$AuthenticationNetworkClient {
  const AuthenticationNetworkClient._() : super();

  const factory AuthenticationNetworkClient.signInWithGoogle(
      {required String uuid, required String idToken}) = _Google;
  const factory AuthenticationNetworkClient.signInWithApple(
      {required String uuid, required String appleId}) = _Apple;
  @override
  String get baseURL => baseUrl;

  @override
  Map<String, dynamic>? get body => {
        'uuid': uuid,
        'type': map(signInWithGoogle: (_) => '', signInWithApple: (_) => ''),
        'idToken':
            maybeMap(orElse: () => null, signInWithGoogle: (s) => s.idToken),
        'appleId':
            maybeMap(orElse: () => null, signInWithApple: (s) => s.appleId),
      };

  @override
  Map<String, dynamic> get header => throw UnimplementedError();

  @override
  String get method => maybeMap(
        orElse: () => 'POST',
      );

  @override
  String get path => maybeMap(
        orElse: () => '/auth',
      );

  @override
  Map<String, dynamic>? get queryParameters => maybeMap(
        orElse: () {
          return null;
        },
      );
}
