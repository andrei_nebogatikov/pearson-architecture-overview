// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'auth.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$AuthenticationNetworkClientTearOff {
  const _$AuthenticationNetworkClientTearOff();

  _Google signInWithGoogle({required String uuid, required String idToken}) {
    return _Google(
      uuid: uuid,
      idToken: idToken,
    );
  }

  _Apple signInWithApple({required String uuid, required String appleId}) {
    return _Apple(
      uuid: uuid,
      appleId: appleId,
    );
  }
}

/// @nodoc
const $AuthenticationNetworkClient = _$AuthenticationNetworkClientTearOff();

/// @nodoc
mixin _$AuthenticationNetworkClient {
  String get uuid => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String uuid, String idToken) signInWithGoogle,
    required TResult Function(String uuid, String appleId) signInWithApple,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String uuid, String idToken)? signInWithGoogle,
    TResult Function(String uuid, String appleId)? signInWithApple,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Google value) signInWithGoogle,
    required TResult Function(_Apple value) signInWithApple,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Google value)? signInWithGoogle,
    TResult Function(_Apple value)? signInWithApple,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AuthenticationNetworkClientCopyWith<AuthenticationNetworkClient>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthenticationNetworkClientCopyWith<$Res> {
  factory $AuthenticationNetworkClientCopyWith(
          AuthenticationNetworkClient value,
          $Res Function(AuthenticationNetworkClient) then) =
      _$AuthenticationNetworkClientCopyWithImpl<$Res>;
  $Res call({String uuid});
}

/// @nodoc
class _$AuthenticationNetworkClientCopyWithImpl<$Res>
    implements $AuthenticationNetworkClientCopyWith<$Res> {
  _$AuthenticationNetworkClientCopyWithImpl(this._value, this._then);

  final AuthenticationNetworkClient _value;
  // ignore: unused_field
  final $Res Function(AuthenticationNetworkClient) _then;

  @override
  $Res call({
    Object? uuid = freezed,
  }) {
    return _then(_value.copyWith(
      uuid: uuid == freezed
          ? _value.uuid
          : uuid // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$GoogleCopyWith<$Res>
    implements $AuthenticationNetworkClientCopyWith<$Res> {
  factory _$GoogleCopyWith(_Google value, $Res Function(_Google) then) =
      __$GoogleCopyWithImpl<$Res>;
  @override
  $Res call({String uuid, String idToken});
}

/// @nodoc
class __$GoogleCopyWithImpl<$Res>
    extends _$AuthenticationNetworkClientCopyWithImpl<$Res>
    implements _$GoogleCopyWith<$Res> {
  __$GoogleCopyWithImpl(_Google _value, $Res Function(_Google) _then)
      : super(_value, (v) => _then(v as _Google));

  @override
  _Google get _value => super._value as _Google;

  @override
  $Res call({
    Object? uuid = freezed,
    Object? idToken = freezed,
  }) {
    return _then(_Google(
      uuid: uuid == freezed
          ? _value.uuid
          : uuid // ignore: cast_nullable_to_non_nullable
              as String,
      idToken: idToken == freezed
          ? _value.idToken
          : idToken // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Google extends _Google {
  const _$_Google({required this.uuid, required this.idToken}) : super._();

  @override
  final String uuid;
  @override
  final String idToken;

  @override
  String toString() {
    return 'AuthenticationNetworkClient.signInWithGoogle(uuid: $uuid, idToken: $idToken)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Google &&
            (identical(other.uuid, uuid) ||
                const DeepCollectionEquality().equals(other.uuid, uuid)) &&
            (identical(other.idToken, idToken) ||
                const DeepCollectionEquality().equals(other.idToken, idToken)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(uuid) ^
      const DeepCollectionEquality().hash(idToken);

  @JsonKey(ignore: true)
  @override
  _$GoogleCopyWith<_Google> get copyWith =>
      __$GoogleCopyWithImpl<_Google>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String uuid, String idToken) signInWithGoogle,
    required TResult Function(String uuid, String appleId) signInWithApple,
  }) {
    return signInWithGoogle(uuid, idToken);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String uuid, String idToken)? signInWithGoogle,
    TResult Function(String uuid, String appleId)? signInWithApple,
    required TResult orElse(),
  }) {
    if (signInWithGoogle != null) {
      return signInWithGoogle(uuid, idToken);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Google value) signInWithGoogle,
    required TResult Function(_Apple value) signInWithApple,
  }) {
    return signInWithGoogle(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Google value)? signInWithGoogle,
    TResult Function(_Apple value)? signInWithApple,
    required TResult orElse(),
  }) {
    if (signInWithGoogle != null) {
      return signInWithGoogle(this);
    }
    return orElse();
  }
}

abstract class _Google extends AuthenticationNetworkClient {
  const factory _Google({required String uuid, required String idToken}) =
      _$_Google;
  const _Google._() : super._();

  @override
  String get uuid => throw _privateConstructorUsedError;
  String get idToken => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$GoogleCopyWith<_Google> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$AppleCopyWith<$Res>
    implements $AuthenticationNetworkClientCopyWith<$Res> {
  factory _$AppleCopyWith(_Apple value, $Res Function(_Apple) then) =
      __$AppleCopyWithImpl<$Res>;
  @override
  $Res call({String uuid, String appleId});
}

/// @nodoc
class __$AppleCopyWithImpl<$Res>
    extends _$AuthenticationNetworkClientCopyWithImpl<$Res>
    implements _$AppleCopyWith<$Res> {
  __$AppleCopyWithImpl(_Apple _value, $Res Function(_Apple) _then)
      : super(_value, (v) => _then(v as _Apple));

  @override
  _Apple get _value => super._value as _Apple;

  @override
  $Res call({
    Object? uuid = freezed,
    Object? appleId = freezed,
  }) {
    return _then(_Apple(
      uuid: uuid == freezed
          ? _value.uuid
          : uuid // ignore: cast_nullable_to_non_nullable
              as String,
      appleId: appleId == freezed
          ? _value.appleId
          : appleId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Apple extends _Apple {
  const _$_Apple({required this.uuid, required this.appleId}) : super._();

  @override
  final String uuid;
  @override
  final String appleId;

  @override
  String toString() {
    return 'AuthenticationNetworkClient.signInWithApple(uuid: $uuid, appleId: $appleId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Apple &&
            (identical(other.uuid, uuid) ||
                const DeepCollectionEquality().equals(other.uuid, uuid)) &&
            (identical(other.appleId, appleId) ||
                const DeepCollectionEquality().equals(other.appleId, appleId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(uuid) ^
      const DeepCollectionEquality().hash(appleId);

  @JsonKey(ignore: true)
  @override
  _$AppleCopyWith<_Apple> get copyWith =>
      __$AppleCopyWithImpl<_Apple>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String uuid, String idToken) signInWithGoogle,
    required TResult Function(String uuid, String appleId) signInWithApple,
  }) {
    return signInWithApple(uuid, appleId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String uuid, String idToken)? signInWithGoogle,
    TResult Function(String uuid, String appleId)? signInWithApple,
    required TResult orElse(),
  }) {
    if (signInWithApple != null) {
      return signInWithApple(uuid, appleId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Google value) signInWithGoogle,
    required TResult Function(_Apple value) signInWithApple,
  }) {
    return signInWithApple(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Google value)? signInWithGoogle,
    TResult Function(_Apple value)? signInWithApple,
    required TResult orElse(),
  }) {
    if (signInWithApple != null) {
      return signInWithApple(this);
    }
    return orElse();
  }
}

abstract class _Apple extends AuthenticationNetworkClient {
  const factory _Apple({required String uuid, required String appleId}) =
      _$_Apple;
  const _Apple._() : super._();

  @override
  String get uuid => throw _privateConstructorUsedError;
  String get appleId => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$AppleCopyWith<_Apple> get copyWith => throw _privateConstructorUsedError;
}
