import 'package:bloc_example/src/constants.dart';
import 'package:bloc_example/src/domain/network/interfaces/base_client_generator.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'event.freezed.dart';

@freezed
class EventNetworkClient extends BaseClientGenerator with _$EventNetworkClient {
  const EventNetworkClient._() : super();

  const factory EventNetworkClient.myEvents() = _MyEvents;
  @override
  String get baseURL => baseUrl;

  @override
  Map<String, dynamic>? get body => maybeWhen(
        orElse: () {
          return null;
        },
      );

  @override
  Map<String, dynamic> get header => throw UnimplementedError();

  @override
  String get method => when(
        myEvents: () => 'GET',
      );

  @override
  String get path => when(
        myEvents: () => 'user/events',
      );

  @override
  Map<String, dynamic>? get queryParameters => maybeWhen(
        orElse: () {
          return null;
        },
      );
}
