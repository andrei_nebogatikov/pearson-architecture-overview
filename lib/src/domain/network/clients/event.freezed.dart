// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$EventNetworkClientTearOff {
  const _$EventNetworkClientTearOff();

  _MyEvents myEvents() {
    return const _MyEvents();
  }
}

/// @nodoc
const $EventNetworkClient = _$EventNetworkClientTearOff();

/// @nodoc
mixin _$EventNetworkClient {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() myEvents,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? myEvents,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_MyEvents value) myEvents,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_MyEvents value)? myEvents,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EventNetworkClientCopyWith<$Res> {
  factory $EventNetworkClientCopyWith(
          EventNetworkClient value, $Res Function(EventNetworkClient) then) =
      _$EventNetworkClientCopyWithImpl<$Res>;
}

/// @nodoc
class _$EventNetworkClientCopyWithImpl<$Res>
    implements $EventNetworkClientCopyWith<$Res> {
  _$EventNetworkClientCopyWithImpl(this._value, this._then);

  final EventNetworkClient _value;
  // ignore: unused_field
  final $Res Function(EventNetworkClient) _then;
}

/// @nodoc
abstract class _$MyEventsCopyWith<$Res> {
  factory _$MyEventsCopyWith(_MyEvents value, $Res Function(_MyEvents) then) =
      __$MyEventsCopyWithImpl<$Res>;
}

/// @nodoc
class __$MyEventsCopyWithImpl<$Res>
    extends _$EventNetworkClientCopyWithImpl<$Res>
    implements _$MyEventsCopyWith<$Res> {
  __$MyEventsCopyWithImpl(_MyEvents _value, $Res Function(_MyEvents) _then)
      : super(_value, (v) => _then(v as _MyEvents));

  @override
  _MyEvents get _value => super._value as _MyEvents;
}

/// @nodoc

class _$_MyEvents extends _MyEvents {
  const _$_MyEvents() : super._();

  @override
  String toString() {
    return 'EventNetworkClient.myEvents()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _MyEvents);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() myEvents,
  }) {
    return myEvents();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? myEvents,
    required TResult orElse(),
  }) {
    if (myEvents != null) {
      return myEvents();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_MyEvents value) myEvents,
  }) {
    return myEvents(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_MyEvents value)? myEvents,
    required TResult orElse(),
  }) {
    if (myEvents != null) {
      return myEvents(this);
    }
    return orElse();
  }
}

abstract class _MyEvents extends EventNetworkClient {
  const factory _MyEvents() = _$_MyEvents;
  const _MyEvents._() : super._();
}
