import 'package:dio/dio.dart';
import 'error.dart';

class EmptyResult implements Exception {}

class NetworkDecoding {
  static var shared = NetworkDecoding();

  K decode<T, K>(
      {required Response<dynamic> response,
      required T Function(Map<String, dynamic>) parser}) {
    try {
      if (response.data['message'] != 'ok') {
        throw ApiError(response.data['error']);
      }
      final d = response.data;
      if (d == null) {
        throw EmptyResult();
      }
      if (d is List) {
        // ignore: unnecessary_cast
        var list = d as List;
        var dataList =
            List<T>.from(list.map<T>((e) => parser(e)).toList()) as K;
        return dataList;
      } else {
        var data = parser(d) as K;
        return data;
      }
    } on TypeError {
      rethrow;
    }
  }
}
