import 'package:bloc_example/src/constants.dart';
import 'package:bloc_example/src/domain/models/user.dart';
import 'package:bloc_example/src/domain/network/clients/auth.dart';
import 'package:bloc_example/src/domain/network/network_executor.dart';
import 'package:bloc_example/src/domain/repositories/authentication.dart';
import 'package:bloc_example/src/domain/repositories/user.dart';
import 'package:bloc_example/src/domain/services/storage.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'authentication_state.dart';

class AuthenticationCubit extends Cubit<AuthenticationState> {
  AuthenticationCubit(this._authenticationRepository, this._userRepository)
      : super(const AuthenticationState.unknown()) {
    trySavedLogin();
  }

  final IAuthenticationRepository _authenticationRepository;
  final IUserRepository _userRepository;

  final api = NetworkExecuter.shared;
  final storage = serviceLocator<Storage>();

  Future<void> trySavedLogin() async {
    await Future.delayed(const Duration(milliseconds: 200));
    final token = storage.get(authSaveKey);
    if (token != null && token.isNotEmpty) {
      final user = await _userRepository.getUser();
      user.map(
          failure: (_) => emit(const AuthenticationState.unauthenticated()),
          success: (result) =>
              emit(AuthenticationState.authenticated(result.data)));
    } else {
      emit(const AuthenticationState.unauthenticated());
    }
  }

  Future<void> logIn({required AuthenticationNetworkClient client}) async {
    // final res = await api.execute<User, User>(
    //     route: client, parser: (e) => User.fromJson(e));
    final res = await _authenticationRepository.logIn(client: client);
    res.map(success: (result) {
      final data = result.data;
      storage.saveLogin(token: data.token, refreshToken: data.refreshToken);
      emit(AuthenticationState.authenticated(data.user));
    }, failure: (_) {
      emit(const AuthenticationState.unauthenticated());
    });
  }

  void logOut() {
    emit(const AuthenticationState.unauthenticated());
  }
}
