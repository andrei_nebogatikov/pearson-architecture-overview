import 'dart:async';

import 'package:bloc_example/src/domain/models/user.dart';
import 'package:bloc_example/src/domain/network/clients/auth.dart';
import 'package:bloc_example/src/domain/network/error.dart';
import 'package:bloc_example/src/domain/network/network_executor.dart';
import 'package:bloc_example/src/domain/network/result.dart';

enum AuthenticationStatus {
  unknown,
  authenticated,
  unauthenticated,
  authenticatedFirstTime
}

abstract class IAuthenticationRepository {
  Future<Result<UserPayload, NetworkError>> logIn(
      {required AuthenticationNetworkClient client});
}

class AuthencticationRepository implements IAuthenticationRepository {
  @override
  Future<Result<UserPayload, NetworkError>> logIn(
      {required AuthenticationNetworkClient client}) {
    return NetworkExecuter.shared.execute<UserPayload, UserPayload>(
        route: client,
        parser: (e) {
          return UserPayload(
              token: e['token'],
              refreshToken: e['refreshToken'],
              user: User.fromJson(e));
        });
  }
}

class MockAuthenticationRepository implements IAuthenticationRepository {
  @override
  Future<Result<UserPayload, NetworkError>> logIn(
      {required AuthenticationNetworkClient client}) async {
    await Future.delayed(const Duration(milliseconds: 1000));
    return Result.success(UserPayload(
        token: 'token', refreshToken: 'refreshToken', user: User.empty));
  }
}
