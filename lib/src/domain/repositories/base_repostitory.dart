import 'package:bloc_example/src/constants.dart';
import 'package:bloc_example/src/domain/network/netowork_creator.dart';
import 'package:bloc_example/src/domain/network/network_executor.dart';
import 'package:bloc_example/src/domain/services/storage.dart';
import 'package:dio/dio.dart';

abstract class BaseRepository {
  final api = NetworkExecuter.shared;
  final storage = serviceLocator<Storage>();

  Dio get client => serviceLocator<NetworkCreator>().client;
}
