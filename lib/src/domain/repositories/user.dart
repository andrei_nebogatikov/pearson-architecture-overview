import 'package:bloc_example/src/domain/models/models.dart';
import 'package:bloc_example/src/domain/network/error.dart';
import 'package:bloc_example/src/domain/network/result.dart';

abstract class IUserRepository {
  Future<Result<User, NetworkError>> getUser();
}

class MockUserRepository implements IUserRepository {
  @override
  Future<Result<User, NetworkError>> getUser() async {
    await Future.delayed(const Duration(milliseconds: 1000));
    return const Result.success(User.empty);
  }
}
