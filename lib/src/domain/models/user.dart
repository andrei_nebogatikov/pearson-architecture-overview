import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';
part 'user.g.dart';

@freezed
class User with _$User {
  const factory User({
    required String learnerId,
  }) = _User;

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  static const empty = User(learnerId: '-1');
}

class UserPayload {
  final String token;
  final String refreshToken;
  final User user;

  UserPayload(
      {required this.token, required this.refreshToken, required this.user});
}
