import 'package:bloc_example/src/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Storage {
  final SharedPreferences _prefs;

  const Storage(this._prefs);

  static Future<Storage> create() async {
    final prefs = await SharedPreferences.getInstance();
    return Storage(prefs);
  }

  Future<void> saveLogin(
      {required String token, required String refreshToken}) async {
    _prefs.setString(authSaveKey, token);
    _prefs.setString(authSaveKey, token);
  }

  Future<bool> save(key, value) async {
    return _prefs.setString(key, value);
  }

  Future<bool> saveBool(key, value) async {
    return _prefs.setBool(key, value);
  }

  bool? getBool(key) {
    return _prefs.getBool(key);
  }

  String? get(key) {
    return _prefs.getString(key);
  }

  Future<bool> delete(key) async {
    return _prefs.remove(key);
  }
}
