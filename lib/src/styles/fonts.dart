import 'package:flutter/material.dart';

import 'colors.dart';

class AppFonts {
  static const titleLargeBold = TextStyle(fontSize: 32, color: AppColors.primaryColor);
}
