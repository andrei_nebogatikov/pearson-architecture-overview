import 'package:flutter/material.dart';

import 'colors.dart';
import 'fonts.dart';

final theme = ThemeData(
    appBarTheme: const AppBarTheme(
        color: AppColors.primaryColor, titleTextStyle: AppFonts.titleLargeBold),
    primaryColor: AppColors.primaryColor);
