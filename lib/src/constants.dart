import 'package:get_it/get_it.dart';

final serviceLocator = GetIt.I;
const authSaveKey = 'auth_token';
const baseUrl = 'https://v-mate.herokuapp.com/';