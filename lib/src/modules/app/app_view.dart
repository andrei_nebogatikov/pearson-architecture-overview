import 'package:bloc_example/src/constants.dart';
import 'package:bloc_example/src/domain/blocs/authentication/authentication.dart';
import 'package:bloc_example/src/domain/blocs/authentication/bloc/authentication_cubit.dart';
import 'package:bloc_example/src/domain/repositories/authentication.dart';
import 'package:bloc_example/src/modules/home/home.dart';
import 'package:bloc_example/src/modules/login/login.dart';
import 'package:bloc_example/src/styles/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AppView extends StatefulWidget {
  const AppView({Key? key}) : super(key: key);

  @override
  _AppViewState createState() => _AppViewState();
}

class _AppViewState extends State<AppView> {
  final _navigatorKey = GlobalKey<NavigatorState>();

  NavigatorState get _navigator => _navigatorKey.currentState!;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: serviceLocator.allReady(),
        builder: (context, snapshot) {
          return MaterialApp(
              restorationScopeId: 'app',
              navigatorKey: _navigatorKey,
              supportedLocales: const [Locale('en'), Locale('ru')],
              // LOCALIZATION
              //
              // localizationsDelegates: const [
              //   AppLocalizations.delegate,
              //   GlobalMaterialLocalizations.delegate,
              //   GlobalWidgetsLocalizations.delegate,
              //   GlobalCupertinoLocalizations.delegate,
              // ],
              // onGenerateTitle: (BuildContext context) =>
              //     AppLocalizations.of(context)!.appTitle,
              debugShowCheckedModeBanner: false,
              title: 'Flutter Demo',
              theme: theme,
              builder: (context, child) {
                return BlocListener<AuthenticationCubit, AuthenticationState>(
                  listener: (context, state) {
                    final status = state.status;
                    switch (status) {
                      case AuthenticationStatus.authenticatedFirstTime:
                      case AuthenticationStatus.authenticated:
                        _navigator.pushReplacement(HomePage.route);
                        break;

                      case AuthenticationStatus.unknown:
                      case AuthenticationStatus.unauthenticated:
                        _navigator.pushReplacement(LoginPage.route);
                        break;
                    }
                  },
                  child: child,
                );
              },
              onGenerateRoute: (_) {
                return MaterialPageRoute(
                    builder: (context) => const SplashPage());
              });
        });
  }
}

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
