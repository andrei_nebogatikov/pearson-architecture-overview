import 'package:bloc_example/src/domain/blocs/authentication/bloc/authentication_cubit.dart';
import 'package:bloc_example/src/domain/repositories/authentication.dart';
import 'package:bloc_example/src/domain/repositories/user.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'app_view.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
            create: (context) => AuthenticationCubit(
                MockAuthenticationRepository(), MockUserRepository()))
      ],
      child: const AppView(),
    );
  }
}
