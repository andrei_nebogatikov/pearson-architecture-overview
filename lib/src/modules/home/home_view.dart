import 'package:bloc_example/src/domain/blocs/blocs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('Home page'),
            ElevatedButton(
                onPressed: () {
                  context.read<AuthenticationCubit>().logOut();
                },
                child: const Text('Logout'))
          ],
        ),
      ),
    );
  }
}
