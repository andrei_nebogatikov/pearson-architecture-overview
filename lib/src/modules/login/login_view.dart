import 'package:bloc_example/src/modules/login/bloc/login_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginView extends StatelessWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: BlocBuilder<LoginCubit, LoginState>(
          builder: (context, state) {
            return state.map(
                success: (_) => const SizedBox(),
                error: (_) => const Center(
                      child: Text('error'),
                    ),
                initial: (_) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                          onPressed: () {
                            const uuid = '';
                            const appleId = '';
                            context
                                .read<LoginCubit>()
                                .loginWithApple(appleId: appleId, uuid: uuid);
                          },
                          child: const Text('apple')),
                      ElevatedButton(
                          onPressed: () {
                            const idToken = '';
                            context
                                .read<LoginCubit>()
                                .loginWithGoogle(idToken: idToken);
                          },
                          child: const Text('google')),
                    ],
                  );
                },
                loading: (_) => const Center(
                      child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation(Colors.red)),
                    ));
          },
        ),
      ),
    );
  }
}
