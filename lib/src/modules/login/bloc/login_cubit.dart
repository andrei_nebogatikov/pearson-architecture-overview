import 'package:bloc_example/src/domain/blocs/blocs.dart';
import 'package:bloc_example/src/domain/network/clients/auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_cubit.freezed.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit(this._authenticationCubit) : super(const _Initial());

  final AuthenticationCubit _authenticationCubit;

  Future<void> _login({required AuthenticationNetworkClient client}) async {
    emit(const _Loading());
    await _authenticationCubit.logIn(client: client);
    emit(const _Success());
  }

  Future<void> loginWithGoogle({required String idToken}) async {
    await _login(
        client: AuthenticationNetworkClient.signInWithGoogle(
            uuid: '', idToken: idToken));
  }

  Future<void> loginWithApple(
      {required String appleId, required String uuid}) async {
    await _login(
        client: AuthenticationNetworkClient.signInWithApple(
            uuid: uuid, appleId: appleId));
  }
}

@freezed
class LoginState with _$LoginState {
  const factory LoginState.initial() = _Initial;
  const factory LoginState.loading() = _Loading;
  const factory LoginState.error() = _Error;
  const factory LoginState.success() = _Success;
}
