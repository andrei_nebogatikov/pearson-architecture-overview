# Architecture setup
This is demo project that describes [Pearson Turing Flutter](https://gitlab.com/pearsontechnology/aips/aidamath/front-end/pearson-turing-flutter) architecture
## Contents
- [Depencies](#depencies)
- [State overview](#state-overview)
- [Networking](#networking)
- [Putting all together](#putting-all-together)
- [Folder structure](#folder-structure)

## Dependencies
- **flutter_bloc** - state management
- **dio** - http client
- **freezed** - model code generation(from/to Json, copyWith, immuatability)
- **git_it** - dependency injection

## State overview
we decided to use BLoC 
> Bloc makes it easy to separate presentation from business logic, making your code fast, easy to test, and reusable.

![Bloc overview](https://bloclibrary.dev/assets/bloc_architecture_full.png)

#### How to use
For creating a state logic unit you need two things:
- Bloc
- State that will be managed by bloc

There are two possible ways to create Bloc
- Cubit
- Bloc

I suggest using Cubit whenever is possible, as its reduce the amount of boilerplate code and easies to use\
example ([link to code](https://gitlab.com/andrei_nebogatikov/pearson-architecture-overview/-/blob/master/lib/src/modules/login/bloc/login_cubit.dart#L8)):
```dart
class LoginCubit extends Cubit<LoginState> {
  LoginCubit(this._authenticationCubit) : super(const _Initial());

  final AuthenticationCubit _authenticationCubit;

  Future<void> _login({required AuthenticationNetworkClient client}) async {
    emit(const _Loading());
    await _authenticationCubit.logIn(client: client);
    emit(const _Success());
  }

  Future<void> loginWithGoogle({required String idToken}) async {
    await _login(
        client: AuthenticationNetworkClient.signInWithGoogle(
            uuid: '', idToken: idToken));
  }

  Future<void> loginWithApple(
      {required String appleId, required String uuid}) async {
    await _login(
        client: AuthenticationNetworkClient.signInWithApple(
            uuid: uuid, appleId: appleId));
  }
}

```
But sometimes Cubit is not enough for nice and smooth state management, this is when Bloc comes in\
[example](https://gitlab.com/andrei_nebogatikov/pearson-architecture-overview/-/blob/master/lib/src/domain/blocs/authentication/bloc/authentication_cubit.dart#L13) where Cubit breaks down
Cubit definition
```dart
class AuthenticationCubit extends Cubit<AuthenticationState> {
  AuthenticationCubit(this._authenticationRepository, this._userRepository)
      : super(const AuthenticationState.unknown()) {
    trySavedLogin();
  }

  final IAuthenticationRepository _authenticationRepository;
  final IUserRepository _userRepository;

  final api = NetworkExecuter.shared;
  final storage = serviceLocator<Storage>();

  Future<void> trySavedLogin() async {
    await Future.delayed(Duration(milliseconds: 200));
    final token = storage.get(authSaveKey);
    if (token != null && token.isNotEmpty) {
      final user = await _userRepository.getUser();
      user.map(
          failure: (_) => emit(const AuthenticationState.unauthenticated()),
          success: (result) =>
              emit(AuthenticationState.authenticated(result.data)));
    } else {
      print(state);
      emit(AuthenticationState.unauthenticated());
    }
  }

  Future<void> logIn({required AuthenticationNetworkClient client}) async {
    final res = await _authenticationRepository.logIn(client: client);
    res.map(success: (result) {
      final data = result.data;
      storage.saveLogin(token: data.token, refreshToken: data.refreshToken);
      emit(AuthenticationState.authenticated(data.user));
    }, failure: (_) {
      emit(const AuthenticationState.unauthenticated());
    });
  }

  void logOut() {
    emit(const AuthenticationState.unauthenticated());
  }
}
```
And the usage
```dart
BlocListener<AuthenticationCubit, AuthenticationState>(
    listener: (context, state) {
        final status = state.status;
        print(status);Ï
        switch (status) {
        case AuthenticationStatus.authenticated:
            _navigator.pushReplacement(HomePage.route);
            break;

        case AuthenticationStatus.unknown:
        case AuthenticationStatus.unauthenticated:
            _navigator.pushReplacement(LoginPage.route);
            break;
        }
    },
    child: child,
);
```
As you can see in _trySavedLogin function there is a magical `await Future.delayed(Duration(milliseconds: 200))`. Without it, our BlocListener wouldn't react to our first state emitting, and we would never get any screen. So this is needed just to wait until our widget is fully set up and then push the new events to the stream. The issue is related to different state emission model in Cubit rather than Bloc.\
In Blocs state emits asynchronously, so according to how and when the event loop completes futures, this means that every state push happens only after building every widget in the screen. On the other side, Cubit's `emit` function works synchronously and any new state immediately would be pushed to the stream, and some BlocListeners can miss events that happened right after Cubit initializing

Some other cases:
- when you need tracing
Bloc allows you to know the sequence of state changes as well as exactly what triggered those changes
- when you need more control over events emitting
Another area in which Bloc excels over Cubit is when we need to take advantage of reactive operators such as `buffer`, `debounceTime`, `throttle`, etc
With Bloc, we can override transformEvents to change the way incoming events are processed by the Bloc.
```dart
@override
Stream<Transition<CounterEvent, int>> transformEvents(
  Stream<CounterEvent> events,
  TransitionFunction<CounterEvent, int> transitionFn,
) {
  return super.transformEvents(
    events.debounceTime(const Duration(milliseconds: 300)),
    transitionFn,
  );
}
```
The state is the actual state that bloc manages, the common way to define states in the bloc is the following
```dart
abstract class State extends Equatable  {}

class Initial extends State {}

class Loading extends State {}

class Loaded extends State {
    final Data data;

    Loaded(this.data);

    @override
    List<Object> get props => [data];
}
```
> `Equatable` needed to override `hash` and `equals`, because Bloc wouldn't emit state if the new state equals to the previous

and the usage
```dart
builder: (context, state) {
    if (state is Loading) {
        return LoadingWidget()
    } else if (state is Loaded) {
        return LoadedWidget(data: state.data);
    }
    return const SizedBox();
}
```
Seems good, but there are some drawbacks:
- A lot of boilerplate
- You can forget to handle some state

and the `freezed` comes to the rescue
As I said earlier freezed is used to generate model classes, but it has one amazing feature that fits perfectly with Bloc - Union classes\
Using this we can rewrite the previous example to the following
```dart
part 'state.freezed.dart'

@freezed
class State with _$State {
    const factory State.loading() = _Loading();
    const factory State.initial() = _Initial();
    const factory State.loaded({ required Data data }) = _Loaded();
}
```
And thats it, but there is more, freezed generates for us `map` (and some others) function that can be used in builder function
```dart
builder: (context, state) => state.map(
    loading: (_) => LoadingWidget(),
    loaded (s) => LoadedWidget(data: s.data),
    initital: (_) => const SizedBox());
```
`map` forces you to define every state handler.

## Networking
The network layer has the following structure

![Network layer](https://miro.medium.com/max/982/1*jERrB0h-5Y9C7Z0Xam2kcw.png)

Each layer has its own error type
- **Request** (Thrown if the request is incorrect)
- **Type** (Thrown in case of error while converting to response model in Decoding layer)
- **Connectivity** (In the Network Executer layer, controls are provided about whether there is an internet connection. If there is no internet connection, it throws an error back)

#### How to use

to use layer you need to define network client and parser that creates a model from JSON output

[example of client](https://gitlab.com/andrei_nebogatikov/pearson-architecture-overview/-/blob/master/lib/src/domain/network/clients/auth.dart#L8)
```dart
@freezed
class AuthenticationNetworkClient extends BaseClientGenerator
    with _$AuthenticationNetworkClient {
  const AuthenticationNetworkClient._() : super();

  const factory AuthenticationNetworkClient.signInWithGoogle(
      {required String uuid, required String idToken}) = _Google;
  const factory AuthenticationNetworkClient.signInWithApple(
      {required String uuid, required String appleId}) = _Apple;
  @override
  String get baseURL => baseUrl;

  @override
  Map<String, dynamic>? get body => {
        'uuid': uuid,
        'type': map(signInWithGoogle: (_) => 'google_auth', signInWithApple: (_) => 'appleid'),
        'idToken':
            maybeMap(orElse: () => null, signInWithGoogle: (s) => s.idToken),
        'appleId':
            maybeMap(orElse: () => null, signInWithApple: (s) => s.appleId),
      };

  @override
  Map<String, dynamic> get header => throw UnimplementedError();

  @override
  String get method => maybeMap(
        orElse: () => 'POST',
      );

  @override
  String get path => maybeMap(
        orElse: () => '/auth',
      );

  @override
  Map<String, dynamic>? get queryParameters => maybeMap(
        orElse: () {
          return null;
        },
      );
}
```
to make a request just call execute method
```dart
NetworkExecutor.shared.execute<User, User>(route: AuthenticationNetworkClient.signInWithGoogle(idToken: 'token'), parser: (e) => User.fromJson(e))
```
> first parameter defines the model type where the second defines the actual return type
> for example if you want to retrieve List, the signature would be the following - `execute<Model, List<Model>>(...)`

the return type Result<T, NetworkError> can be handled like this
```dart
final res = await _request();
res.map(
    succes: (result) {
        emit(SuccessState(result.dart))
    },
    failure: (result) {
        emit(_ErrorState(result.error.localizedMessage));
        // or you can define custom logic for each type of error via result.error.map
    }
)
```
Nice and clean

The common way to work with data in Blocs is using repositories - dumb classes that fetches data from somewhere(remote or local)\
in our case they just proxy requests to Network layer
```dart
class DataRepository {
    Future<Result<User, NetworkError>> getUser() {
        return NetworkExecutor.shared.execute<User, User>(...);
    }
}
```
So the above `await _request()` should be moved onto the Repository method call

As our blocs don't care where the data came from, we are able to easily switch between different data sources, and it allows us to mock our repositories during the development

## Putting all together
With all of the above, we are getting the following steps to create a new screen
1. generate models via freezed
2. create State using freezed Union classes
(optional: create Events for Bloc)
3. Create a Repository to fetch data needed
4. create Cubit/Bloc and define all of the business logic
5. Use cubit/bloc in UI to respond to the events

And that's it, you're good to go

## Folder structure
With all of the above in mind, I came to the following structure
```
lib/
  - modules/ - this is where stay all application's screens(app, login, home etc)
  - domain/ - all business logic staff goes here
    - models/ - shared models
    - repositories/ - common repositories (auth for example)
    - services/ - notifications, database connections and so on
    - blocs/ - all blocs that related to global app state like auth, locale, cache
  - widgets/ - shared widgets
  - styles/ staff that is related to theming (colors, fonts, const border radius, paddings etc)
    - theme.dart - main point where all theme data combines in ThemeData that will be used in MaterialApp
```
Each module has the following structure
```
module/
  bloc/
    - bloc.dart
    - bloc_state.dart
    - bloc_repository.dart
    - (optional) bloc_events.dart
  widgets/ - widgets that exists only on that screen
    ...
    - widgets.dart - exports all widgets at once
  - module_page.dart - injects bloc in the current context
  - module_view.dart - ui
```
